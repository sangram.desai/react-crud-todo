
import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
    constructor(props) {
        super(props);

        this.onChangeTaskName = this.onChangeTaskName.bind(this);
        this.onChangeTaskStatus = this.onChangeTaskStatus.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            taskName: '',
            isComplete: false,
        }
    }

    componentDidMount() {
        axios.get('http://localhost:1337/todo/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    taskName: response.data.taskName,
                    isComplete: response.data.isComplete
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeTaskName(e) {
        this.setState({
            taskName: e.target.value
        });
    }
    onChangeTaskStatus(e) {
        console.log("onChangeTaskStatus", e.target.checked);
        this.setState({
            isComplete: e.target.checked
        })
    }


    onSubmit(e) {
        e.preventDefault();
        const obj = {
            taskName: this.state.taskName,
            isComplete: this.state.isComplete,
        };
        console.log("onSubmit", obj);
        axios.put('http://localhost:1337/todo/' + this.props.match.params.id, obj)
            .then(res => console.log(res.data));



        setTimeout(() => {
            this.props.history.push('/index');
        }, 700)
    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3 align="center">Update Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Task Name:  </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.taskName}
                            onChange={this.onChangeTaskName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Is Completed: </label>
                        <input type="checkbox"
                            className="form-control"
                            checked={this.state.isComplete}
                            onChange={this.onChangeTaskStatus}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit"
                            value="Update Task"
                            className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}