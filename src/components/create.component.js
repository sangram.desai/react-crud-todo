import React, { Component } from 'react';
import axios from 'axios';

export default class Create extends Component {

    constructor(props) {
        super(props);

        this.onChangeTaskName = this.onChangeTaskName.bind(this);
        this.onChangeTaskStatus = this.onChangeTaskStatus.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            taskName: '',
            isComplete: false,
        }
    }

    onChangeTaskName(e) {
        this.setState({
            taskName: e.target.value
        });
    }
    onChangeTaskStatus(e) {
        this.setState({
            isComplete: e.target.checked
        })
    }

    onSubmit(e) {
        e.preventDefault();
        console.log(`The values are ${this.state.taskName}, ${this.state.isComplete}`)


        const obj = {
            taskName: this.state.taskName,
            isComplete: this.state.isComplete
        };
        axios.post('http://localhost:1337/todo', obj)
            .then(res => console.log(res.data));

        this.setState({
            taskName: '',
            isComplete: false
        })
    }


    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3>Add New Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Task Name:  </label>
                        <input type="text" value={this.state.taskName} className="form-control" onChange={this.onChangeTaskName} />
                    </div>
                    <div className="form-group">
                        <label>Is Task Completed:  </label>
                        <input type="checkbox" style={{ width: "30px" }} value={this.state.isComplete} className="form-control"
                            onChange={this.onChangeTaskStatus} />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Save Todo" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}